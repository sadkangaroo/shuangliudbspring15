#include <iostream>
#include <stdlib.h>
#include <memory.h>

#include "hfpage.h"
#include "heapfile.h"
#include "buf.h"
#include "db.h"


// **********************************************************
// page class constructor

void HFPage::init(PageId pageNo)
{
    curPage = pageNo;
    prevPage = nextPage = INVALID_PAGE;
    slotCnt = 0;
    freeSpace = sizeof(data) + sizeof(slot_t);
    usedPtr = sizeof(data);
}

// **********************************************************
// dump page utlity
void HFPage::dumpPage()
{
    int i;

    cout << "dumpPage, this: " << this << endl;
    cout << "curPage= " << curPage << ", nextPage=" << nextPage << endl;
    cout << "usedPtr=" << usedPtr << ",  freeSpace=" << freeSpace
         << ", slotCnt=" << slotCnt << endl;
   
    for (i=0; i < slotCnt; i++) {
        cout << "slot["<< i <<"].offset=" << slot[i].offset
             << ", slot["<< i << "].length=" << slot[i].length << endl;
    }
}

// **********************************************************
PageId HFPage::getPrevPage()
{
    return prevPage;
}

// **********************************************************
void HFPage::setPrevPage(PageId pageNo)
{
    prevPage = pageNo;
}

// **********************************************************
void HFPage::setNextPage(PageId pageNo)
{
    nextPage = pageNo;
}

// **********************************************************
PageId HFPage::getNextPage()
{
    return nextPage;
}

// **********************************************************
// Add a new record to the page. Returns OK if everything went OK
// otherwise, returns DONE if sufficient space does not exist
// RID of the new record is returned via rid parameter.

Status HFPage::insertRecord(char* recPtr, int recLen, RID& rid)
{
    if (available_space() < recLen) {
        return DONE;
    }
    int slotNow = -1;
    for (int i = 0; i < slotCnt; ++i) {
        if (slot[i].length == EMPTY_SLOT) {
            slotNow = i;
            break;
        }
    }
    if (slotNow == -1) {
        slotNow = slotCnt;
        slot[slotCnt].offset = usedPtr - recLen;
        slot[slotCnt].length = recLen;
        slotCnt++;
        freeSpace -= sizeof(slot_t);
    }
    usedPtr -= recLen;
    freeSpace -= recLen;
    memmove(data + usedPtr, recPtr, recLen);
    rid.pageNo = curPage;
    rid.slotNo = slotNow;
    return OK;
}

// **********************************************************
// Delete a record from a page. Returns OK if everything went okay.
// Compacts remaining records but leaves a hole in the slot array.
// Use memmove() rather than memcpy() as space may overlap.
Status HFPage::deleteRecord(const RID& rid)
{
    if (rid.pageNo != curPage || rid.slotNo >= slotCnt || rid.slotNo < 0 || slot[rid.slotNo].length == EMPTY_SLOT) {
        return FAIL;
    }
    int len = slot[rid.slotNo].length;
    int offset = slot[rid.slotNo].offset;
    for (int i = 0; i < slotCnt; ++i) {
        if (slot[i].offset < offset) {
            slot[i].offset += len;
        }
    }
    slot[rid.slotNo].length = EMPTY_SLOT;
    while (slotCnt > 0 && slot[slotCnt - 1].length == EMPTY_SLOT) {
        slotCnt--;
        freeSpace += sizeof(slot_t);
    }
    memmove(data + usedPtr + len, data + usedPtr, offset - usedPtr); 
    freeSpace += len;
    usedPtr += len;
    return OK;
}

// **********************************************************
// returns RID of first record on page
Status HFPage::firstRecord(RID& firstRid)
{
    for (int i = 0; i < slotCnt; ++i) {
        if (slot[i].length != EMPTY_SLOT) {
            firstRid.pageNo = curPage;
            firstRid.slotNo = i;
            return OK;
        }
    }
    return DONE;
}

// **********************************************************
// returns RID of next record on the page
// returns DONE if no more records exist on the page; otherwise OK
Status HFPage::nextRecord (RID curRid, RID& nextRid)
{
    if (curRid.pageNo != curPage || curRid.slotNo >= slotCnt || curRid.slotNo < 0 || slot[curRid.slotNo].length == EMPTY_SLOT) {
        return FAIL;
    }
    for (int i = curRid.slotNo + 1; i < slotCnt; ++i) {
        if (slot[i].length != EMPTY_SLOT) {
            nextRid.pageNo = curPage;
            nextRid.slotNo = i;
            return OK;
        }
    }
    return DONE;
}

// **********************************************************
// returns length and copies out record with RID rid
Status HFPage::getRecord(RID rid, char* recPtr, int& recLen)
{
    if (rid.pageNo != curPage || rid.slotNo >= slotCnt || rid.slotNo < 0 || slot[rid.slotNo].length == EMPTY_SLOT) {
        return FAIL;
    }
    recLen = slot[rid.slotNo].length;
    memmove(recPtr, data + slot[rid.slotNo].offset, recLen);
    return OK;
}

// **********************************************************
// returns length and pointer to record with RID rid.  The difference
// between this and getRecord is that getRecord copies out the record
// into recPtr, while this function returns a pointer to the record
// in recPtr.
Status HFPage::returnRecord(RID rid, char*& recPtr, int& recLen)
{
    if (rid.pageNo != curPage || rid.slotNo >= slotCnt || rid.slotNo < 0 || slot[rid.slotNo].length == EMPTY_SLOT) {
        return FAIL;
    }
    recLen = slot[rid.slotNo].length;
    recPtr = data + slot[rid.slotNo].offset;
    return OK;
}

// **********************************************************
// Returns the amount of available space on the heap file page
int HFPage::available_space(void)
{
    int slotNow = -1;
    for (int i = 0; i < slotCnt; ++i) {
        if (slot[i].length == EMPTY_SLOT) {
            slotNow = i;
            break;
        }
    }
    if (slotNow == -1) {
        return freeSpace - sizeof(slot_t);
    }
    else return freeSpace;
}

// **********************************************************
// Returns 1 if the HFPage is empty, and 0 otherwise.
// It scans the slot directory looking for a non-empty slot.
bool HFPage::empty(void)
{
    return usedPtr == sizeof(data);
}



