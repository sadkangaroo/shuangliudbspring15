/*
 * btleaf_page.cc - implementation of class BTLeafPage
 *
 */

#include "btleaf_page.h"

const char* BTLeafErrorMsgs[] = {
};

static error_string_table btree_table(BTLEAFPAGE, BTLeafErrorMsgs);

bool BTLeafPage:: hasKey(const void *key, AttrType key_type) {
    RID curRid;
    Keytype curKey;
    RID dataRid;
    Status status = get_first(curRid, &curKey, dataRid);
    while (status == OK) {
      if (keyCompare(key, &curKey, key_type) == 0) {
          return true;
      }
      status = get_next(curRid, &curKey, dataRid);
    }
    return false;
}
   
Status BTLeafPage::insertRec(const void *key,
                              AttrType key_type,
                              RID dataRid,
                              RID& rid)
{
  KeyDataEntry recPtr;
  Datatype data;
  data.rid = dataRid;
  int recLen;
  if (!hasKey(key, key_type)) {
      make_entry(&recPtr, key_type, key, LEAF, data, &recLen);
      if (insertRecord(key_type, (char*)(&recPtr), recLen, rid) != OK) {
          return FAIL;
      }
  }
  return OK;
}

Status BTLeafPage::get_data_rid(void *key,
                                AttrType key_type,
                                RID & dataRid)
{
  Status status;
  RID curRid;
  Keytype curKey;
  status = get_first(curRid, &curKey, dataRid);
  while (status == OK) {
      if (keyCompare(key, &curKey, key_type) == 0) {
          return OK;
      }
      status = get_next(curRid, &curKey, dataRid);
  }
  return FAIL;
}

Status BTLeafPage::get_first (RID& rid,
                              void *key,
                              RID & dataRid)
{ 
    Status status;
    status = firstRecord(rid);
    if (status == DONE) {
        return NOMORERECS;
    }
    else {
      char* recPtr;
      int recLen;
      returnRecord(rid, recPtr, recLen);
      Datatype data;
      get_key_data(key, &data, (KeyDataEntry*) recPtr, recLen, LEAF);
      dataRid = data.rid;
      return OK;
    }
}

Status BTLeafPage::get_next (RID& rid,
                             void *key,
                             RID & dataRid)
{
    Status status;
    RID curRid = rid;
    status = nextRecord(curRid, rid);
    if (status == DONE) {
        return NOMORERECS;
    }
    else {
      char* recPtr;
      int recLen;
      returnRecord(rid, recPtr, recLen);
      Datatype data;
      get_key_data(key, &data, (KeyDataEntry*) recPtr, recLen, LEAF);
      dataRid = data.rid;
      return OK;
    }
}
