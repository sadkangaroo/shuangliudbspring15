/*****************************************************************************/
/*************** Implementation of the Buffer Manager Layer ******************/
/*****************************************************************************/


#include "buf.h"

// Define buffer manager error messages here
enum bufErrCodes  {NO_CANDIDATE, CANNOT_UNPIN, CANNOT_FREEPAGE};

// Define error message here
static const char* bufErrMsgs[] = { 
    "No available frames in the buffer", 
    "Cannot unpin the page",
    "Cannot free the page"
};

// Create a static "error_string_table" object and register the error messages
// with minibase system 
static error_string_table bufTable(BUFMGR,bufErrMsgs);

BufMgr::BufMgr (int numbuf, Replacer *replacer) {
    size = numbuf;
    bufPool = new Page[numbuf];
    descriptors = new Descriptor[numbuf];
    loveQueue.init();
    hateQueue.init();
    hash.init();
    for (int i = 0; i < numbuf; ++i) {
        descriptors[i] = Descriptor();
    }
    for (int i = 0; i < numbuf; ++i) {
        loveQueue.insert(i);
        descriptors[i].inLove = loveQueue.head;
        descriptors[i].pageNum = -1;
    }
}

//*************************************************************
//** This is the implementation of ~BufMgr
//************************************************************
BufMgr::~BufMgr() {
    for (int i = 0; i < size; ++i) {
        flushPage(descriptors[i].pageNum);
    }
    hash.clear();
    loveQueue.clear();
    hateQueue.clear();
    delete[] bufPool;
    delete[] descriptors;
}

int BufMgr::findReplacement() {
    if (hateQueue.head) {
        return hateQueue.head->frameNum;
    }
    else if (loveQueue.tail) {
        return loveQueue.tail->frameNum;
    }
    else {
        return -1;
    }
}

Status BufMgr::pinPage(PageId PageId_in_a_DB, Page*& page, int emptyPage) {
    int frameNum = hash.find(PageId_in_a_DB);
    if (frameNum != -1) {
        descriptors[frameNum].pinCount++;
        if (descriptors[frameNum].pinCount == 1) {
            loveQueue.remove(descriptors[frameNum].inLove);
            hateQueue.remove(descriptors[frameNum].inHate);
            descriptors[frameNum].inLove = descriptors[frameNum].inHate = 0;
        }
        page = bufPool + frameNum;
        return OK;
    }
    frameNum = findReplacement();
    if (frameNum == -1) {
        return MINIBASE_FIRST_ERROR(BUFMGR, NO_CANDIDATE);
    }
    if (descriptors[frameNum].dirtyBit) {
        flushPage(descriptors[frameNum].pageNum);
    }
    hash.remove(descriptors[frameNum].pageNum);
    loveQueue.remove(descriptors[frameNum].inLove);
    hateQueue.remove(descriptors[frameNum].inHate);
    page = bufPool + frameNum;
    Status status = MINIBASE_DB->read_page(PageId_in_a_DB, page);
    if (status != OK) {
        return MINIBASE_CHAIN_ERROR(BUFMGR, status);
    }
    descriptors[frameNum] = Descriptor(PageId_in_a_DB, 1, 0, 0);
    hash.insert(descriptors[frameNum].pageNum, frameNum);
    return OK;
}

Status BufMgr::newPage(PageId& firstPageId, Page*& firstpage, int howmany) {
    int frameNum = findReplacement(); 
    if (frameNum == -1) {
        return MINIBASE_FIRST_ERROR(BUFMGR, NO_CANDIDATE);
    }
    Status status = MINIBASE_DB->allocate_page(firstPageId, howmany);
    if (status != OK) {
        return MINIBASE_CHAIN_ERROR(BUFMGR, status);
    }
    return pinPage(firstPageId, firstpage);
}

Status BufMgr::flushPage(PageId pageid) {
    int frameNum = hash.find(pageid); 
    if (descriptors[frameNum].dirtyBit == false) {
        return OK;
    }
    descriptors[frameNum].dirtyBit = false;
    Status status = MINIBASE_DB->write_page(pageid, bufPool + frameNum);
    if (status != OK) {
        return MINIBASE_CHAIN_ERROR(BUFMGR, status);
    }
    else {
        return OK;
    }
}


//*************************************************************
//** This is the implementation of unpinPage
//************************************************************

Status BufMgr::unpinPage(PageId page_num, int dirty=FALSE, int hate = FALSE){
    int frameNum = hash.find(page_num);
    if (frameNum == -1 || descriptors[frameNum].pinCount == 0) {
        return MINIBASE_FIRST_ERROR(BUFMGR, CANNOT_UNPIN);
    }
    if (dirty) {
        descriptors[frameNum].dirtyBit = TRUE;
    }
    descriptors[frameNum].pinCount--;
    if (descriptors[frameNum].pinCount == 0) {
        if (hate) {
            if (!descriptors[frameNum].inLove) {
                hateQueue.insert(frameNum);
                descriptors[frameNum].inHate = hateQueue.head;
            }
        }
        else {
            loveQueue.insert(frameNum);
            descriptors[frameNum].inLove = loveQueue.head;
            if (descriptors[frameNum].inHate) {
                hateQueue.remove(descriptors[frameNum].inHate);
                descriptors[frameNum].inHate = 0;
            }
        }
    }
    return OK;
}

//*************************************************************
//** This is the implementation of freePage
//************************************************************

Status BufMgr::freePage(PageId globalPageId){
    int frameNum = hash.find(globalPageId);
    if (descriptors[frameNum].pinCount) {
        return MINIBASE_FIRST_ERROR(BUFMGR, CANNOT_FREEPAGE);
    }
    Status status = MINIBASE_DB->deallocate_page(globalPageId);
    if (status != OK) {
        return MINIBASE_CHAIN_ERROR(BUFMGR, status);
    }
    return OK;
}

Status BufMgr::flushAllPages(){
    for (int i = 0; i < size; ++i) {
        flushPage(descriptors[i].pageNum);
    }
    return OK;
}
