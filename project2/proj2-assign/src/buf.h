///////////////////////////////////////////////////////////////////////////////
/////////////  The Header File for the Buffer Manager /////////////////////////
///////////////////////////////////////////////////////////////////////////////


#ifndef BUF_H
#define BUF_H

#include "db.h"
#include "page.h"


#define NUMBUF 20   
// Default number of frames, artifically small number for ease of debugging.

#define HTSIZE 7
#define HTA 3
#define HTB 5
// Hash Table size
//You should define the necessary classes and data structures for the hash table, 
// and the queues for LSR, MRU, etc.


/*******************ALL BELOW are purely local to buffer Manager********/

// You should create enums for internal errors in the buffer manager.
//enum bufErrCodes  { 
//};

class Replacer;

struct ListRecord {
    int frameNum;
    ListRecord *pre, *next;
    ListRecord() {}
    ListRecord(int _frameNum, ListRecord* _pre, ListRecord* _next) {
        frameNum = _frameNum;
        pre = _pre;
        next = _next;
    }
};

struct List {
    ListRecord *head, *tail;
    List() {
        init();
    }
    void init() {
        head = tail = 0;
    }
    void remove(ListRecord *rec) {
        if (rec == 0) {
            return;
        }
        if (head == rec) {
            if (tail == rec) {
                head = tail = 0;
            }
            else {
                head = rec->next;
                head->pre = 0;
            }
        }
        else {
            rec->pre->next = rec->next;
            if (rec->next) {
                rec->next->pre = rec->pre;
            }
            else {
                tail = rec->pre;
            }
        }
        delete rec;
    }
    void insert(int frameNum) {
        ListRecord* now;
        if (head == 0) {
            now = new ListRecord(frameNum, 0, 0);
            head = tail = now;
        }
        else {
            now = new ListRecord(frameNum, 0, head);
            head->pre = now;
            head = now;
        }
    }
    void outputAll() {
        ListRecord* now = head;
        while (now) {
            printf("%d ", now->frameNum);
            now = now->next;
        }
        printf("\n");
    }
    void clear() {
        while (head) {
            ListRecord *pre = head;
            head = head->next;
            delete pre;
        }
    }
};

struct Descriptor {
    int pinCount;
    PageId pageNum;
    bool dirtyBit;
    ListRecord* inLove;
    ListRecord* inHate;
    Descriptor() {
        dirtyBit = false;
        inLove = inHate = 0;
    }
    Descriptor(PageId _pageNum, int _pinCount, ListRecord* _inLove, ListRecord* _inHate) {
        pageNum = _pageNum;
        pinCount = _pinCount;
        inLove = _inLove;
        inHate = _inHate;
    }
};

struct HashRecord {
    PageId pageNum;
    int frameNum;
    HashRecord* next;
    HashRecord() {}
    HashRecord(PageId _pageNum, int _frameNum, HashRecord* _next) {
        pageNum = _pageNum;
        frameNum = _frameNum;
        next = _next;
    }
};

struct HashTable {
    HashRecord* table[HTSIZE];
    HashTable() {
        init();
    }
    void init() {
        for (int i = 0; i < HTSIZE; ++i) {
            table[i] = 0;
        }
    }
    int find(PageId pageNum) {
        int bucket = (HTA * pageNum + HTB) % HTSIZE;
        HashRecord* now = table[bucket];
        while (now) {
            if (now->pageNum == pageNum) {
                return now->frameNum;
            }
            now = now->next;
        }
        return -1;
    }
    void insert(PageId pageNum, PageId frameNum) {
        int bucket = (HTA * pageNum + HTB) % HTSIZE;
        table[bucket] = new HashRecord(pageNum, frameNum, table[bucket]);
    }
    void remove(PageId pageNum) {
        if (pageNum == -1) return;
        int bucket = (HTA * pageNum + HTB) % HTSIZE;
        HashRecord *now = table[bucket], *pre = 0;
        while (now) {
            if (now->pageNum == pageNum) {
                if (pre == 0) {
                    table[bucket] = now->next;
                }
                else {
                    pre->next = now->next;
                }
                delete now;
                return;
            }
            pre = now;
            now = now->next;
        }
    }

    void clear() {
        for (int i = 0; i < HTSIZE; ++i) {
            while (table[i]) {
                HashRecord *cur = table[i];
                table[i] = table[i]->next;
                delete cur;
            }
        }
    }
};

class BufMgr {

    private: 

        int size;
        Descriptor* descriptors;
        HashTable hash;
        List loveQueue, hateQueue;

        int findReplacement();

    public:

        Page* bufPool; // The actual buffer pool

        BufMgr (int numbuf, Replacer *replacer = 0); 
        // Initializes a buffer manager managing "numbuf" buffers.
        // Disregard the "replacer" parameter for now. In the full 
        // implementation of minibase, it is a pointer to an object
        // representing one of several buffer pool replacement schemes.

        ~BufMgr();           // Flush all valid dirty pages to disk

        Status pinPage(PageId PageId_in_a_DB, Page*& page, int emptyPage=0);
        // Check if this page is in buffer pool, otherwise
        // find a frame for this page, read in and pin it.
        // also write out the old page if it's dirty before reading
        // if emptyPage==TRUE, then actually no read is done to bring
        // the page

        Status unpinPage(PageId globalPageId_in_a_DB, int dirty, int hate);
        // hate should be TRUE if the page is hated and FALSE otherwise
        // if pincount>0, decrement it and if it becomes zero,
        // put it in a group of replacement candidates.
        // if pincount=0 before this call, return error.

        Status newPage(PageId& firstPageId, Page*& firstpage, int howmany=1); 
        // call DB object to allocate a run of new pages and 
        // find a frame in the buffer pool for the first page
        // and pin it. If buffer is full, ask DB to deallocate 
        // all these pages and return error

        Status freePage(PageId globalPageId); 
        // user should call this method if it needs to delete a page
        // this routine will call DB to deallocate the page 

        Status flushPage(PageId pageid);
        // Used to flush a particular page of the buffer pool to disk
        // Should call the write_page method of the DB class

        Status flushAllPages();
        // Flush all pages of the buffer pool to disk, as per flushPage.

        /* DO NOT REMOVE THIS METHOD */    
        Status unpinPage(PageId globalPageId_in_a_DB, int dirty=FALSE)
            //for backward compatibility with the libraries
        {
            return unpinPage(globalPageId_in_a_DB, dirty, FALSE);
        }
};

#endif
